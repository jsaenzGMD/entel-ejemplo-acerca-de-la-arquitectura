package pe.entel.android.acerosarequipa.data.repository;

import android.content.Context;

import pe.entel.android.acerosarequipa.data.repository.datasource.application.UtilApplicationStore;
import pe.entel.android.acerosarequipa.domain.repository.UtilRepository;

/**
 * Created by rtamayov on 10/04/2017.
 */
public class UtilDataRepository implements UtilRepository {

    final Context context;

    public UtilDataRepository(Context context) {
        this.context = context;
    }

    @Override
    public boolean haySenal() {
        UtilApplicationStore store = new UtilApplicationStore(context);
        return store.haySenal();
    }

}
