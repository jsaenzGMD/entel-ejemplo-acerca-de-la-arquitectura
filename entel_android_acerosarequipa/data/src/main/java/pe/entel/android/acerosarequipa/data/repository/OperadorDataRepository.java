package pe.entel.android.acerosarequipa.data.repository;

import android.content.Context;

import pe.entel.android.acerosarequipa.domain.repository.OperadorRepository;
import pe.entel.android.util.verificacionaplicacion.datasource.OperadorApplicationStore;

/**
 * Created by rtamayov on 07/04/2017.
 */
public class OperadorDataRepository implements OperadorRepository {

    final Context context;

    public OperadorDataRepository(Context context) {
        this.context = context;
    }

    @Override
    public boolean validarOperador() {
        OperadorApplicationStore store = new OperadorApplicationStore(context);
        return store.validarOperador();
    }


}
