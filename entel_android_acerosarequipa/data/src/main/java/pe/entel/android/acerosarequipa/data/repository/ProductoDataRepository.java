package pe.entel.android.acerosarequipa.data.repository;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import pe.entel.android.acerosarequipa.data.dao.ProductoDao;
import pe.entel.android.acerosarequipa.data.repository.datasource.local.ProductoLocalStore;
import pe.entel.android.acerosarequipa.domain.entity.Producto;
import pe.entel.android.acerosarequipa.domain.repository.ProductoRepository;

/**
 * Como los datos se obtendrán desde el SQLite nos se tienen callbacks o listeners asociados
 * a esta clase para devolver alguna respuesta hacia las otras clases
 */

public class ProductoDataRepository implements ProductoRepository {
    @Override
    public List<Producto> listarProductosXNombre(String nombre) {
        /**
         * Dentro de DATA - REPOSITORY - LOCAL se encuentran los métodos para obtener los datos
         */
        ProductoLocalStore store = new ProductoLocalStore();
        ModelMapper modelMapper = new ModelMapper();
        Type lisType = new TypeToken<List<Producto>>() {}.getType();
        /**
         * listarProductoXNombreLocalStore(nombre), envio del parámetro nombre el cual permitirá traer los datos en forma de una lista luego de ser mapeados.
         */
        List<ProductoDao> productoDaos = store.listarProductoXNombreLocalStore(nombre);
        /**
         * Permitirá mapear los datos obtenidos con el objeto ProductoDao hacia un objeto Producto.
         */
        return modelMapper.map(productoDaos,lisType);
    }

    @Override
    public List<Producto> listarProductos() {
        ProductoLocalStore store = new ProductoLocalStore();
        ModelMapper modelMapper = new ModelMapper();
        Type listType = new TypeToken<List<Producto>>() {}.getType();

        List<ProductoDao> productoDaos = store.listarProductoLocalStore();
        /**
         * Permitirá mapear los datos obtenidos con el objeto ProductoDao hacia un objeto Producto.
         */
        return modelMapper.map(productoDaos,listType);
    }
}
