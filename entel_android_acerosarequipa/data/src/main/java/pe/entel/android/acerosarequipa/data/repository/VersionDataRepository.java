package pe.entel.android.acerosarequipa.data.repository;

import android.content.Context;

import pe.entel.android.acerosarequipa.data.exception.RepositoryErrorBundle;
import pe.entel.android.acerosarequipa.data.repository.datasource.cloud.VersionCloudStore;
import pe.entel.android.acerosarequipa.data.repository.datasource.preference.VersionPreferenceStore;
import pe.entel.android.acerosarequipa.domain.repository.VersionRepository;

/**
 * Created on 10/04/2017.
 */
public class VersionDataRepository implements VersionRepository {

    final Context context;

    public VersionDataRepository(Context context) {
        this.context = context;
    }

    @Override
    public void obtenerVersion(final ObtenerVersionCallback obtenerVersionCallback) {

        VersionCloudStore versionCloudStore = new VersionCloudStore(context);
        versionCloudStore.obtenerVersion(new VersionCloudStore.ObtenerVersionCallback() {
            @Override
            public void obtenida(String version) {
                obtenerVersionCallback.obtenida(version);
            }

            @Override
            public void onError(Exception e) {
                obtenerVersionCallback.onError(new RepositoryErrorBundle(e));
            }
        });

    }

    @Override
    public void setVersion(String version) {
        VersionPreferenceStore store =  new VersionPreferenceStore(context);
        store.setVersion(version);
    }

    @Override
    public String getVersion() {
        VersionPreferenceStore store =  new VersionPreferenceStore(context);
        return store.getVersion();
    }
}
