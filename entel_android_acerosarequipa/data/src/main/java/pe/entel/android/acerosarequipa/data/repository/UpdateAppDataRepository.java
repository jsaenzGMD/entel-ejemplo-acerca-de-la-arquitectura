package pe.entel.android.acerosarequipa.data.repository;

import android.content.Context;

import pe.entel.android.acerosarequipa.data.exception.RepositoryErrorBundle;
import pe.entel.android.acerosarequipa.domain.repository.UpdateAppRepository;
import pe.entel.android.util.actualizacionaplicacion.datasource.UpdateAppCloudStore;

/**
 * Created by rtamayov on 11/04/2017.
 */
public class UpdateAppDataRepository implements UpdateAppRepository {

    final Context context;

    public UpdateAppDataRepository(Context context) {
        this.context = context;
    }


    @Override
    public void updateApp(String url, final UpdateAppCallback updateAppCallback) {
        UpdateAppCloudStore store = new UpdateAppCloudStore(context);
        store.getApk(url, new UpdateAppCloudStore.UpdateAppCallback() {
            @Override
            public void onApkGot() {
                updateAppCallback.onUpdatedApp();
            }

            @Override
            public void onError(Exception e) {
                updateAppCallback.onError(new RepositoryErrorBundle(e));
            }
        });
    }
}
