package pe.entel.android.acerosarequipa.data.repository.datasource.local;

import java.util.List;

import pe.entel.android.acerosarequipa.data.dao.ProductoDao;
import pe.entel.android.acerosarequipa.domain.entity.Producto;

/**
 * Métodos que permiten la extracción de datos desde el SQLite utilizando Sugar ORM
 */

public class ProductoLocalStore {
    /**
     * Permite listar productos con laguna condicional
     * @param nombre
     * @return
     */
    public List<ProductoDao> listarProductoXNombreLocalStore(String nombre){
        return ProductoDao.find(ProductoDao.class,"nombre like '%"+nombre+"%'");
    }

    /**
     * Permite el listado del productos sin alguna condicional, obtener el contenido de la tabla
     * @return
     */
    public List<ProductoDao> listarProductoLocalStore(){
        return ProductoDao.listAll(ProductoDao.class);
    }
}
