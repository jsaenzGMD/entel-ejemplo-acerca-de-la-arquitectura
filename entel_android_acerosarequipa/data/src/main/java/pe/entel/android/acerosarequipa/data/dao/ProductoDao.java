package pe.entel.android.acerosarequipa.data.dao;

import com.orm.SugarRecord;

/**
 * Permite la creación de la tabla con los elementos, para ello se debe extender de "SugarRecord".
 */

public class ProductoDao extends SugarRecord{
    private String idProducto;
    private String codigoProducto;
    private String nombre;
    private String precio;

    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public String getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }
}
