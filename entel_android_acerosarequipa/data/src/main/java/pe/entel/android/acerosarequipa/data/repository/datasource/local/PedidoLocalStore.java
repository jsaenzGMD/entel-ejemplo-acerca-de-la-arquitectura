package pe.entel.android.acerosarequipa.data.repository.datasource.local;

import java.util.List;

import pe.entel.android.acerosarequipa.data.dao.PedidoDao;
import pe.entel.android.acerosarequipa.data.dao.PedidoDetalleDao;
import pe.entel.android.acerosarequipa.domain.entity.Pedido;

/**
 * Created by jsaenz on 8/11/2017.
 */

public class PedidoLocalStore {

    public void guardarPedido(PedidoDao pedidoDao){
        pedidoDao.save();
    }

    public List<PedidoDao> listarPedidosPendientes(){
        return PedidoDao.find(PedidoDao.class,"flg_enviado = 'F'");
    }

    public void guardarProducto(PedidoDetalleDao pedidoDetalleDao) {
        pedidoDetalleDao.save();
    }

    public List<PedidoDetalleDao> listarPedidoDetalle(Long idPedido){
        return PedidoDetalleDao.find(PedidoDetalleDao.class,"id_pedido = ?", String.valueOf(idPedido));
    }
}
