package pe.entel.android.acerosarequipa.data.repository.datasource.local;

import android.util.Log;

import com.orm.SugarRecord;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import pe.entel.android.acerosarequipa.data.dao.ProductoDao;

/**
 * Created by jsaenz on 8/11/2017.
 */

public class MaestroLocalStore {
    public void borrarTodo(){
        ProductoDao.deleteAll(ProductoDao.class);
    }

    public void insertaQuery(InputStream inputStream){
        try{
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while((line = bufferedReader.readLine()) != null){
                Log.v("MaestroLocalStore",line);
                SugarRecord.executeQuery(line);
            }

        }catch (Exception e){
            Log.e("MaestroLocalStore",e.getMessage());
        }
    }
}
