package pe.entel.android.acerosarequipa.data.repository;

import android.content.Context;

import java.io.InputStream;

import pe.entel.android.acerosarequipa.data.exception.RepositoryErrorBundle;
import pe.entel.android.acerosarequipa.data.repository.datasource.cloud.MaestroCloudStore;
import pe.entel.android.acerosarequipa.data.repository.datasource.local.MaestroLocalStore;
import pe.entel.android.acerosarequipa.domain.repository.MaestroRespository;

/**
 * Created by jsaenz on 8/11/2017.
 */

public class MaestroDataRepository implements MaestroRespository {
    Context context;

    /**
     * Permite recibir el contexto de la aplicación
     * @param context
     */
    public MaestroDataRepository(Context context) {
        this.context = context;
    }


    @Override
    public void sincronizarMaestros(String usuario, final SincronizarMaestrosCallbackRepository sincronizarMaestrosCallback) {
        MaestroCloudStore maestroCloudStore = new MaestroCloudStore(context);
        maestroCloudStore.sincronizarMaestros(usuario, new MaestroCloudStore.SincronizarMaestrosCallback() {
            /**
             * Se recibe la respuesta luego de realizar el consumo del webservice
             * @param mensaje
             * @param inputStream
             */
            @Override
            public void onSincronizacion(String mensaje, InputStream inputStream) {
                /**
                 * Si se realizó la sincronización correctamente se procederá a ingresar el QUERY ó los datos obtenidos hacia el SQLite
                 */
                MaestroLocalStore maestroLocalStore = new MaestroLocalStore();
                maestroLocalStore.insertaQuery(inputStream);
                sincronizarMaestrosCallback.onSincronizado(mensaje);

            }

            @Override
            public void onError(Exception e) {
                sincronizarMaestrosCallback.onError(new RepositoryErrorBundle(e));
            }
        });

    }

    @Override
    public void borrarTodo() {
        MaestroLocalStore store = new MaestroLocalStore();
        store.borrarTodo();
    }
}
