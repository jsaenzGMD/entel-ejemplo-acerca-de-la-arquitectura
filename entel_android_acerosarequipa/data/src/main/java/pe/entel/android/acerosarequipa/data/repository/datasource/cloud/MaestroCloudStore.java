package pe.entel.android.acerosarequipa.data.repository.datasource.cloud;

import android.content.Context;
import android.util.Log;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import okhttp3.ResponseBody;
import pe.entel.android.acerosarequipa.data.exception.NetworkConnectionException;
import pe.entel.android.acerosarequipa.data.net.Url;
import pe.entel.android.acerosarequipa.data.net.message.Response;
import pe.entel.android.acerosarequipa.data.util.UtilitarioData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.QueryMap;

/**
 * Las clases que se encuentran en cloud permitirá realizar la conexión con el servicio
 */

public class MaestroCloudStore {
    Context context;
    Retrofit retrofit;

    public MaestroCloudStore(Context context) {
        this.context = context;
        retrofit = new Retrofit.Builder().baseUrl(Url.obtenerBase(context)).addConverterFactory(GsonConverterFactory.create()).build();
    }

    public void sincronizarMaestros(String usuario, final SincronizarMaestrosCallback callback){
        if(UtilitarioData.isThereInternetConnection(context)){
            MaestroRetroService service =  retrofit.create(MaestroRetroService.class);
            Map<String,String> data = new HashMap<>();
            data.put("usuario",usuario);

            final Call<ResponseBody> responseBodyCall = service.sincronizarMaestros(data);
            Log.v("URL", Url.obtenerRuta(context, Url.EnumUrl.MAESTRO));
            Log.v("usuario", usuario);

            responseBodyCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                    if(response.code() == 200 && response.message() != null){
                        try{
                            okhttp3.Headers headers = response.headers();
                            int idResultado = Integer.parseInt(headers.get("IdResultado"));
                            String resultado = headers.get("Resultado");

                            if(idResultado == Response.CODE_OK ){
                                InputStream inputStream = response.body().byteStream();
                                inputStream = new GZIPInputStream(inputStream);
                                callback.onSincronizacion(resultado,inputStream);
                            }else if (idResultado == Response.CODE_ERROR) {
                                callback.onError(new Exception(resultado));
                            }


                        }catch (Exception e){
                            callback.onError(e);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    callback.onError(new Exception(t.getMessage()));
                }
            });


        }else{
            callback.onError(new NetworkConnectionException("Fuera Cobertura"));
        }

    }

    public interface MaestroRetroService{
        @Headers("Accept-Encoding: gzip")
        @GET("Maestro")
        Call<ResponseBody> sincronizarMaestros(@QueryMap Map<String,String> options);
    }

    public interface SincronizarMaestrosCallback{
        void onSincronizacion(String mensaje, InputStream inputStream);
        void onError(Exception e);

    }
}
