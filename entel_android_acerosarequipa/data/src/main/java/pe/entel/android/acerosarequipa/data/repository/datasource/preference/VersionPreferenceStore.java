package pe.entel.android.acerosarequipa.data.repository.datasource.preference;

import android.content.Context;

import pe.entel.android.acerosarequipa.data.repository.datasource.generic.PreferenceStore;


/**
 * Created by rtamayov on 11/04/2017.
 */
public class VersionPreferenceStore extends PreferenceStore {
    private final static String VERSION="VERSION";

    public VersionPreferenceStore(Context context) {
        super(context);
    }

    public String getVersion() {
        return getPreference(VERSION);
    }

    public void setVersion(String version) {
        saveOnSharePreferences(VERSION,version);
    }
}
