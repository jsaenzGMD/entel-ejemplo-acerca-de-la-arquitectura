package pe.entel.android.acerosarequipa.data.repository;

import android.content.Context;

import pe.entel.android.acerosarequipa.data.exception.RepositoryErrorBundle;
import pe.entel.android.acerosarequipa.data.repository.datasource.cloud.UsuarioCloudStore;
import pe.entel.android.acerosarequipa.data.repository.datasource.preference.UsuarioPreferenceStore;
import pe.entel.android.acerosarequipa.domain.entity.Usuario;
import pe.entel.android.acerosarequipa.domain.repository.UsuarioRepository;

/**
 * Created on 3/11/2017.
 */

public class UsuarioDataRepository implements UsuarioRepository {
    Context context;

    public UsuarioDataRepository(Context context) {
        this.context = context;
    }

    @Override
    public void validarUsuario(String codigo, String clave, final ValidarUsuarioCallback validarUsuarioCallback) {
        UsuarioCloudStore usuarioCloudStore = new UsuarioCloudStore(context);
        usuarioCloudStore.validarUsuario(codigo, clave, new UsuarioCloudStore.ValidarUsuarioCallback() {
            @Override
            public void onValidar(String mensaje, Usuario usuario) {
                validarUsuarioCallback.onValidado(mensaje,usuario);
            }

            @Override
            public void onError(Exception e) {
                validarUsuarioCallback.onError(new RepositoryErrorBundle(e));
            }
        });
    }

    @Override
    public void setUsuario(Usuario usuario) {
        UsuarioPreferenceStore usuarioPreferenceStore = new UsuarioPreferenceStore(context);
        usuarioPreferenceStore.setUsuario(usuario);

    }

    @Override
    public Usuario getUsuario() {
        UsuarioPreferenceStore usuarioPreferenceStore = new UsuarioPreferenceStore(context);
        return usuarioPreferenceStore.getUsuario();
    }
}
