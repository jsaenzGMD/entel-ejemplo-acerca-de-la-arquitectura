package pe.entel.android.acerosarequipa.domain.usercase.implementation;

import java.util.ArrayList;
import java.util.List;

import pe.entel.android.acerosarequipa.domain.entity.Producto;
import pe.entel.android.acerosarequipa.domain.repository.ProductoRepository;
import pe.entel.android.acerosarequipa.domain.usercase.ListarProductoUseCase;

/**
 * Created by jsaenz on 7/11/2017.
 */

public class ListarProductoUseCaseImpl implements ListarProductoUseCase {
    /**
     * Permite declarar la interfaz que permitirá el acceso a los datos del SQLite
     */
    ProductoRepository productoRepository;

    /**
     * Constructor la cual permite la recepción de los métodos conteniendo el acceso a los datos del SQLite.
     * @param productoRepository
     */
    public ListarProductoUseCaseImpl(ProductoRepository productoRepository) {
        this.productoRepository = productoRepository;
    }

    @Override
    public List<Producto> ejecutar(String nombre) {
        /**
         * Permite ejecutar el metodo que permite obtener los datos, y enviarle el parámetro por el cual se relizará el filtrado.
         * En caso se quiera aplicar alguna validación se deberá hacer en esta clase.
         */
        List<Producto> productoList = new ArrayList<>();
        productoList = productoRepository.listarProductosXNombre(nombre);
        return productoList;
    }

    @Override
    public List<Producto> ejecutar(){
        /**
         * Permite obtener la lista total de productos que se encuentra dentro del SQLite
         */
        List<Producto> productos = new ArrayList<>();
        productos = productoRepository.listarProductos();
        return productos;
    }
}
