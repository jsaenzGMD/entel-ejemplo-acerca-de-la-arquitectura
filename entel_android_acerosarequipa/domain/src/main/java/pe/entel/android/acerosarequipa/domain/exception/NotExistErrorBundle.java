package pe.entel.android.acerosarequipa.domain.exception;

import pe.entel.android.acerosarequipa.domain.exception.ErrorBundle;

/**
 * Created by asoto on 12/02/2015.
 */
public class NotExistErrorBundle implements ErrorBundle {

    private String nombre;

    public NotExistErrorBundle(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public Exception getException() {
        return new Exception("Not Exist " + nombre);
    }

    @Override
    public String getErrorMessage() {
        return "Not exist " + nombre;
    }
}
