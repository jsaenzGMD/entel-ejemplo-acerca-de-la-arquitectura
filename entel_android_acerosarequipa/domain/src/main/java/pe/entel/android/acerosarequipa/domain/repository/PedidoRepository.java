package pe.entel.android.acerosarequipa.domain.repository;

import java.util.List;

import pe.entel.android.acerosarequipa.domain.entity.Pedido;
import pe.entel.android.acerosarequipa.domain.exception.ErrorBundle;

/**
 * Permite definir los métodos o funciones que se utilizarán al obtener los datos ya sea del SQLite o desde el Webservice
 */

public interface PedidoRepository {
    void setPedido(Pedido pedido);
    Pedido getPedido();

    void guardarPedido(Pedido pedido);

    List<Pedido> listarPedidosPendientes();
    void enviarPedido(Pedido pedido,EnviarPedidoCallbackRepository enviarPedidoCallbackRepository);

    interface EnviarPedidoCallbackRepository{
        void onEnviado(String mensaje);
        void onError(ErrorBundle errorBundle);
    }
}
