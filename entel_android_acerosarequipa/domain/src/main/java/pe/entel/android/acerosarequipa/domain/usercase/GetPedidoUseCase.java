package pe.entel.android.acerosarequipa.domain.usercase;

import pe.entel.android.acerosarequipa.domain.entity.Pedido;

/**
 * Created by rtamayov on 13/04/2017.
 */

public interface GetPedidoUseCase {

    Pedido ejecutar();
}
