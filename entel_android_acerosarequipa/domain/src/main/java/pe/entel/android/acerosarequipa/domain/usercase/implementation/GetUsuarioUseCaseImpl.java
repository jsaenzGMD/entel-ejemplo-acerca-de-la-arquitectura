package pe.entel.android.acerosarequipa.domain.usercase.implementation;

import pe.entel.android.acerosarequipa.domain.entity.Usuario;
import pe.entel.android.acerosarequipa.domain.repository.UsuarioRepository;
import pe.entel.android.acerosarequipa.domain.usercase.GetUsuarioUseCase;

/**
 * Created by rtamayov on 11/04/2017.
 */
public class GetUsuarioUseCaseImpl implements GetUsuarioUseCase {

    final UsuarioRepository usuarioRepository;

    public GetUsuarioUseCaseImpl(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    @Override
    public Usuario ejecutar() {
        return usuarioRepository.getUsuario();
    }
}
