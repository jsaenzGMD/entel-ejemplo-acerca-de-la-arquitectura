package pe.entel.android.acerosarequipa.domain.usercase;

import pe.entel.android.acerosarequipa.domain.exception.ErrorBundle;

/**
 * Created on 11/04/2017.
 */
public interface ValidarUsuarioUseCase {

    void ejecutar(String codigo, String clave, Callback callback);

    interface Callback {
        void onValidado(String mensaje);

        void onError(ErrorBundle errorBundle);
    }
}
