package pe.entel.android.acerosarequipa.domain.usercase;

/**
 * Created on 07/04/2017.
 */
public interface ValidarOperadorUseCase {
    void ejecutar();
}
