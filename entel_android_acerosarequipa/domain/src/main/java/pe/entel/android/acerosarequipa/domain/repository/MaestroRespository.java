package pe.entel.android.acerosarequipa.domain.repository;

import pe.entel.android.acerosarequipa.domain.exception.ErrorBundle;

/**
 * Created by jsaenz on 8/11/2017.
 */

public interface MaestroRespository {
    /**
     * Para el caso del consumo de webservices a diferencia de obtener los datos desde el SQLite, se deberá hacer
     * la recepción de la información ó validaciones a través de Callbacks.
     * Aquí se implementan los métodos que se deberían usar para el consumo de los webservices.
     * @param usuario
     */

    void sincronizarMaestros(String usuario,SincronizarMaestrosCallbackRepository sincronizarMaestrosCallback);
    void borrarTodo();

    /**
     * Permitirá devolver una respuesta al ejecutar el método el cual lo invoca
     */
    interface SincronizarMaestrosCallbackRepository{
        void onSincronizado(String strResultado);
        void onError(ErrorBundle errorBundle);
    }

}
