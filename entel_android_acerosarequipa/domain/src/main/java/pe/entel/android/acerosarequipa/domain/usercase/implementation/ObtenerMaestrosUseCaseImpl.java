package pe.entel.android.acerosarequipa.domain.usercase.implementation;

import pe.entel.android.acerosarequipa.domain.exception.ErrorBundle;
import pe.entel.android.acerosarequipa.domain.repository.MaestroRespository;
import pe.entel.android.acerosarequipa.domain.usercase.ObtenerMaestrosUseCase;

/**
 * Created by jsaenz on 8/11/2017.
 */

public class ObtenerMaestrosUseCaseImpl implements ObtenerMaestrosUseCase{
    MaestroRespository maestroRespository;


    public ObtenerMaestrosUseCaseImpl(MaestroRespository maestroRespository) {
        this.maestroRespository = maestroRespository;
    }

    @Override
    public void ejecutar(String usuario, final CallbackObtenerMaestrosUseCase callbackObtenerMaestrosUseCase) {
        maestroRespository.borrarTodo();
        maestroRespository.sincronizarMaestros(usuario, new MaestroRespository.SincronizarMaestrosCallbackRepository() {
            @Override
            public void onSincronizado(String strResultado) {
                callbackObtenerMaestrosUseCase.onSincronizado(strResultado);

            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                callbackObtenerMaestrosUseCase.onError(errorBundle);
            }
        });

    }
}
