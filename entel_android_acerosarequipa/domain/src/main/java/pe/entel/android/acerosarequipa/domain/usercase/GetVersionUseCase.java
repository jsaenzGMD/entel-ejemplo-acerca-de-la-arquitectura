package pe.entel.android.acerosarequipa.domain.usercase;

/**
 * Created on 11/04/2017.
 */
public interface GetVersionUseCase {
    String ejecutar();
}
