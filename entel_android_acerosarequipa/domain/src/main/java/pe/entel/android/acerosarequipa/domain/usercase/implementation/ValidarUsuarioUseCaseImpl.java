package pe.entel.android.acerosarequipa.domain.usercase.implementation;

import pe.entel.android.acerosarequipa.domain.entity.Usuario;
import pe.entel.android.acerosarequipa.domain.exception.EncapsulatedErrorBundle;
import pe.entel.android.acerosarequipa.domain.exception.ErrorBundle;
import pe.entel.android.acerosarequipa.domain.repository.UsuarioRepository;
import pe.entel.android.acerosarequipa.domain.usercase.ValidarUsuarioUseCase;

/**
 * Created on 3/11/2017.
 */

public class ValidarUsuarioUseCaseImpl implements ValidarUsuarioUseCase {

    UsuarioRepository usuarioRepository;

    public ValidarUsuarioUseCaseImpl(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    @Override
    public void ejecutar(String codigo, final String clave, final Callback callback) {
        if (codigo.equals("") || clave.equals("")) {
            Exception exception = new Exception("Ingresa usuario y clave");
            ErrorBundle errorBundle = new EncapsulatedErrorBundle(exception);
            callback.onError(errorBundle);
        } else {
            usuarioRepository.validarUsuario(codigo, clave, new UsuarioRepository.ValidarUsuarioCallback() {
                @Override
                public void onValidado(String mensaje, Usuario usuario) {
                    usuario.setClave(clave);
                    /**
                     * Se procede a guardar el usuario logueado en la BDD ó sharedPreference
                     */
                    usuarioRepository.setUsuario(usuario);
                    callback.onValidado(mensaje);
                }

                @Override
                public void onError(ErrorBundle errorBundle) {
                    callback.onError(errorBundle);
                }
            });

        }

    }
}
