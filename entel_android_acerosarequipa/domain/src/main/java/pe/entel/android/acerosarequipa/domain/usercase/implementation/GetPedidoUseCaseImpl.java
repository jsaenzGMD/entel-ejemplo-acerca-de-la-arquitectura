package pe.entel.android.acerosarequipa.domain.usercase.implementation;

import pe.entel.android.acerosarequipa.domain.entity.Pedido;
import pe.entel.android.acerosarequipa.domain.repository.PedidoRepository;
import pe.entel.android.acerosarequipa.domain.usercase.GetPedidoUseCase;

/**
 * Created by rtamayov on 13/04/2017.
 */

public class GetPedidoUseCaseImpl implements GetPedidoUseCase {
    final PedidoRepository pedidoRepository;

    public GetPedidoUseCaseImpl(PedidoRepository pedidoRepository) {
        this.pedidoRepository = pedidoRepository;
    }

    @Override
    public Pedido ejecutar() {
        return pedidoRepository.getPedido();
    }
}
