package pe.entel.android.acerosarequipa.domain.repository;

import pe.entel.android.acerosarequipa.domain.entity.Usuario;
import pe.entel.android.acerosarequipa.domain.exception.ErrorBundle;

/**
 * Created on 11/04/2017.
 */
public interface UsuarioRepository {

    void validarUsuario(String codigo, String clave, ValidarUsuarioCallback validarUsuarioCallback);
    void setUsuario(Usuario usuario);
    Usuario getUsuario();

    interface ValidarUsuarioCallback {
        void onValidado(String mensaje, Usuario usuario);
        void onError(ErrorBundle errorBundle);
    }
}
