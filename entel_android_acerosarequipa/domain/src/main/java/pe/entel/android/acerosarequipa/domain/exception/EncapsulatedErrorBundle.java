package pe.entel.android.acerosarequipa.domain.exception;

import pe.entel.android.acerosarequipa.domain.exception.ErrorBundle;

/**
 * Created by alexander on 14/02/2015.
 */
public class EncapsulatedErrorBundle implements ErrorBundle {

    private Exception error;

    public EncapsulatedErrorBundle(Exception e) {
        this.error = e;
    }

    @Override
    public Exception getException() {
        return error;
    }

    @Override
    public String getErrorMessage() {
        if (error != null)
            return error.getMessage();
        else
            return "No definido";
    }
}
