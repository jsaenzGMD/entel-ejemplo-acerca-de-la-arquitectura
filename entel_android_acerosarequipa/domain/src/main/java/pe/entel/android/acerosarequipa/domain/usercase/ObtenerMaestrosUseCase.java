package pe.entel.android.acerosarequipa.domain.usercase;

import pe.entel.android.acerosarequipa.domain.exception.ErrorBundle;

/**
 * Created by jsaenz on 8/11/2017.
 */

public interface ObtenerMaestrosUseCase {
    void ejecutar(String usuario, CallbackObtenerMaestrosUseCase callbackObtenerMaestros);

    interface CallbackObtenerMaestrosUseCase {
        void onSincronizado(String result);
        void onError(ErrorBundle errorBundle);
    }
}
