package pe.entel.android.acerosarequipa.domain.repository;

import java.util.List;

import pe.entel.android.acerosarequipa.domain.entity.Producto;

/**
 * Interfaz que indica los métodos que se implementarán al obtener los datos, desde el SQLite ó desde el Servicio.
 */

public interface ProductoRepository {
    /**
     * La implementación de éstas funciones se encuentran dentro deL Módulo DATA - REPOSITORY: ProductoDataRepository
     * @param nombre
     * @return
     */
    List<Producto> listarProductosXNombre(String nombre);
    List<Producto> listarProductos();
}
