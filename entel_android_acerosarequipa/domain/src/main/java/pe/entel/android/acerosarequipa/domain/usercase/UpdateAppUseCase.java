package pe.entel.android.acerosarequipa.domain.usercase;


import pe.entel.android.acerosarequipa.domain.exception.ErrorBundle;

/**
 * Created by rtamayov on 03/04/2017.
 */
public interface UpdateAppUseCase {

    void updateApp(String url, Callback useCaseCallback);

    interface Callback {
        void onUpdatedApp();

        void onError(ErrorBundle errorBundle);
    }
}
