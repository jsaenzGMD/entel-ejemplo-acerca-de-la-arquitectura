package pe.entel.android.acerosarequipa.domain.usercase;

import java.util.List;

import pe.entel.android.acerosarequipa.domain.entity.Producto;

/**
 * Se definen los métodos o funciones asociadas a ésta actividad
 */

public interface ListarProductoUseCase {
    /**
     * Permitirá el envío del parámetro para su posterior uso, y a través de éste método acceder a su implementación
     * @param nombre
     * @return
     */
    List<Producto> ejecutar(String nombre);
    List<Producto> ejecutar();
}
