package pe.entel.android.acerosarequipa.domain.repository;

import pe.entel.android.acerosarequipa.domain.exception.ErrorBundle;

/**
 * Created on 10/04/2017.
 */
public interface VersionRepository {
    void obtenerVersion(ObtenerVersionCallback obtenerVersionCallback);
    void setVersion(String version);
    String getVersion();

    interface ObtenerVersionCallback {
        void obtenida(String version);
        void onError(ErrorBundle errorBundle);

    }
}
