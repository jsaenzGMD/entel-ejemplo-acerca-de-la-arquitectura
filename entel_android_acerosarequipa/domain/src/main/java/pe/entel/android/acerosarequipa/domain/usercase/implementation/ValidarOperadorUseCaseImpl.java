package pe.entel.android.acerosarequipa.domain.usercase.implementation;


import pe.entel.android.acerosarequipa.domain.repository.OperadorRepository;
import pe.entel.android.acerosarequipa.domain.repository.UtilRepository;
import pe.entel.android.acerosarequipa.domain.usercase.ValidarOperadorUseCase;

/**
 * Created on 07/04/2017.
 */
public class ValidarOperadorUseCaseImpl implements ValidarOperadorUseCase {

    final OperadorRepository operadorRepository;
    final UtilRepository utilRepository;

    public ValidarOperadorUseCaseImpl(OperadorRepository operadorRepository, UtilRepository utilRepository) {
        this.operadorRepository = operadorRepository;
        this.utilRepository = utilRepository;
    }

    @Override
    public void ejecutar() {
        boolean haySenal = utilRepository.haySenal();

        if(haySenal){
            boolean validado = operadorRepository.validarOperador();
            if(!validado){
                throw new RuntimeException("Operador no valido");
            }
        }

    }
}
