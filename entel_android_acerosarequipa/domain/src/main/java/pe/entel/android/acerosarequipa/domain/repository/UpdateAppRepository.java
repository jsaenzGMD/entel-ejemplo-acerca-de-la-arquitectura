package pe.entel.android.acerosarequipa.domain.repository;

import pe.entel.android.acerosarequipa.domain.exception.ErrorBundle;

/**
 * Created on 03/04/2017.
 */
public interface UpdateAppRepository {

    void updateApp(String url, UpdateAppCallback updateAppCallback);

    interface UpdateAppCallback {
        void onUpdatedApp();

        void onError(ErrorBundle errorBundle);
    }
}
