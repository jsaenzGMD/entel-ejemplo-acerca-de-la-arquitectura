package pe.entel.android.acerosarequipa.domain.usercase;

import pe.entel.android.acerosarequipa.domain.entity.Usuario;

/**
 * Created by rtamayov on 11/04/2017.
 */
public interface GetUsuarioUseCase {
    Usuario ejecutar();
}
