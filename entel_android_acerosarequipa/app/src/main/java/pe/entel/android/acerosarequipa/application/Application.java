package pe.entel.android.acerosarequipa.application;

import android.content.Context;
import android.support.multidex.MultiDex;

import com.orm.SugarApp;

/**
 * Created by jsaenz on 7/11/2017.
 */

public class Application extends SugarApp{
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
