package pe.entel.android.acerosarequipa.view;

/**
 * Created by jsaenz on 7/11/2017.
 */

public interface MenuView {
    /**
     * Permitirá recibir alguna respuesta, a través de esté método se obtendrá la respuesta desde el Presenter hacia el Navigator
     */
    void irPedido();
}
