package pe.entel.android.acerosarequipa.navigation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;

import java.io.Serializable;
import java.util.List;

import pe.entel.android.acerosarequipa.R;
import pe.entel.android.acerosarequipa.domain.entity.Producto;
import pe.entel.android.acerosarequipa.view.activity.MenuActivity;
import pe.entel.android.acerosarequipa.view.activity.OperadorInvalidoActivity;
import pe.entel.android.acerosarequipa.view.activity.PedidoActivity;
import pe.entel.android.acerosarequipa.view.activity.PreferenciasActivity;
import pe.entel.android.acerosarequipa.view.activity.UpdateAppActivity;
import pe.entel.android.controlesentel.busqueda.model.ListaBusqueda;
import pe.entel.android.controlesentel.busqueda.view.activity.BusquedaActivity;

/**
 * Created on 6/11/2017.
 */

public class Navigator {
    public static void navigateToOperadorInvalido(Context context){
        if(context != null){
            Intent intent = OperadorInvalidoActivity.getCallingIntent(context);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
        }
    }

    public static void navigateToPreferencias(Context context, int perfil){
        if(context != null){
            Intent intent = PreferenciasActivity.getCallingIntent(context);
            intent.putExtra(context.getString(R.string.sp_permiso), perfil);
            context.startActivity(intent);
        }
    }

    public static void navigateToUpdateApp(Context context,String isUrlActualizar){
        if(context != null){
            Intent intent = UpdateAppActivity.getCallingIntent(context);
            intent.putExtra("URL_APK",isUrlActualizar);
            context.startActivity(intent);
        }
    }

    public static void navigateToMenu(Context context) {
        if(context != null){
            Intent intent = MenuActivity.getCallingIntent(context);
            ((Activity)context).finish();
            context.startActivity(intent);
        }
    }

    /**
     * Permite la navegación entre activities, a través de este método se podrá ingresar hacia el otro activity.
     * @param context
     */
    public static void navigateToPedido(Context context){
        if(context != null){
            Intent intent = PedidoActivity.getCallingIntent(context);
            //((Activity)context).finish();
            context.startActivity(intent);
        }
    }

    public static void navigateToProductoLista(Context context, List<ListaBusqueda> list, int tipoList){
        //if(context != null){
        //    Intent intent = ProductoListaActivity.getCallingIntent(context);
        //    context.startActivity(intent);
        //}
        if(context != null){
            Intent intent = BusquedaActivity.getCallingIntent(context);
            intent.putExtra(BusquedaActivity.paramList, (Serializable) list);
            intent.putExtra(BusquedaActivity.paramTipoList, tipoList);
            ((Activity)context).startActivityForResult(intent, 101);

        }
    }

}
