package pe.entel.android.acerosarequipa.view.activity;

import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import pe.entel.android.acerosarequipa.R;
import pe.entel.android.acerosarequipa.navigation.Navigator;
import pe.entel.android.acerosarequipa.presenter.LoginPresenter;
import pe.entel.android.acerosarequipa.view.LoginView;

public class LoginActivity extends BaseActivity implements LoginView {

    LoginPresenter presenter;

    @InjectView(R.id.loginUser_txtUsuario)
    TextInputEditText txtUsuario;
    @InjectView(R.id.loginUser_txtPassword)
    TextInputEditText txtClave;
    @InjectView(R.id.login_version)
    TextView txtVersion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.inject(this);
        presenter = new LoginPresenter(this);
        presenter.iniciar();
    }

    @OnClick(R.id.login_btn_ingresar)
    public void validarUsuario() {
        String codigo = txtUsuario.getText().toString();
        String clave = txtClave.getText().toString();
        presenter.validarUsuario(codigo, clave);
    }

    @OnClick(R.id.login_config)
    public void presionarAdministracion() {
        presenter.validarPerfil(txtUsuario.getText().toString(), txtUsuario.getText().toString());
    }

    @Override
    public void actualizarVersion(String version) {
        txtVersion.setText(version);
    }

    @Override
    public void irPreferenciar(int perfil) {
        Navigator.navigateToPreferencias(this,perfil);
    }

    @Override
    public void irMenu() {
        Navigator.navigateToMenu(this);
    }

    @Override
    public void actualizarUsuario(String codigo, String clave) {
        txtUsuario.setText(codigo);
        txtClave.setText(clave);
    }
}
