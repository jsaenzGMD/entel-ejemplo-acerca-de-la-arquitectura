package pe.entel.android.acerosarequipa.presenter;

import java.util.ArrayList;
import java.util.List;

import pe.entel.android.acerosarequipa.R;
import pe.entel.android.acerosarequipa.data.repository.PedidoDataRepository;
import pe.entel.android.acerosarequipa.data.repository.ProductoDataRepository;
import pe.entel.android.acerosarequipa.domain.entity.Pedido;
import pe.entel.android.acerosarequipa.domain.entity.Producto;
import pe.entel.android.acerosarequipa.domain.repository.PedidoRepository;
import pe.entel.android.acerosarequipa.domain.repository.ProductoRepository;
import pe.entel.android.acerosarequipa.domain.usercase.GetPedidoUseCase;
import pe.entel.android.acerosarequipa.domain.usercase.ListarProductoUseCase;
import pe.entel.android.acerosarequipa.domain.usercase.implementation.GetPedidoUseCaseImpl;
import pe.entel.android.acerosarequipa.domain.usercase.implementation.ListarProductoUseCaseImpl;
import pe.entel.android.acerosarequipa.view.activity.PedidoActivity;
import pe.entel.android.controlesentel.busqueda.model.EntidadBusqueda;
import pe.entel.android.controlesentel.busqueda.model.ListaBusqueda;

/**
 * Created by jsaenz on 7/11/2017.
 */

public class PedidoPresenter {
    PedidoActivity view;

    public PedidoPresenter(PedidoActivity view) {
        this.view = view;
    }

    public void irListaProductos() {
        /**
         * Instancia la cual permitirá tener los métodos necesarios para obtener los datos desde el SQLite
         */
        /**
         * ProductoRepository permitirá definir los métodos o funciones que se necesitan para obtener los datos
         * ésta clase se encuentra dentro del "domain", posteriormente se implementará en "Data"
         */
        ProductoRepository productoDataRepository = new ProductoDataRepository();

        /**
         * Se instanciará los casos de uso, éstos serán definidos según el modelo del negocio ó para cada una de las actividades
         * que se realizará en algún proceso.
         */
        ListarProductoUseCase listarProductoUseCase = new ListarProductoUseCaseImpl(productoDataRepository);
        /**
         * Se obtuvo la lista de productos, ésta se almacena en una lista
         */
        List<Producto> productoList = listarProductoUseCase.ejecutar();


        /**
         * Enviando hacia alguna librería o recycler para mostrar los datos
         * Esta parte está enviando hacia la librería de componentes.
         */
        EntidadBusqueda itemEntidadBusqueda;
        List<EntidadBusqueda> listEntidadBusqueda = new ArrayList<>();
        List<ListaBusqueda> list = new ArrayList<>();
        if (productoList != null) {
            for (Producto producto : productoList) {
                itemEntidadBusqueda = new EntidadBusqueda();
                itemEntidadBusqueda.setFlagIndicador_0(true);
                itemEntidadBusqueda.setFlagIndicadorTipoDato_0(1);
                itemEntidadBusqueda.setFlagIndicadorTipoImage_0(false);
                itemEntidadBusqueda.setImageIndicador_0(R.drawable.ic_shopping_blue);
                itemEntidadBusqueda.setFlagIndicadorTitulo(true);
                itemEntidadBusqueda.setTitulo(producto.getNombre());
                itemEntidadBusqueda.setFlagIndicadorDetalle(true);
                itemEntidadBusqueda.setDetalle(producto.getCodigoProducto());
                itemEntidadBusqueda.setFlagLayoutIndicador(false);
                listEntidadBusqueda.add(itemEntidadBusqueda);
            }
        }
        ListaBusqueda itemList1 = new ListaBusqueda();
        itemList1.setTitle("Búsqueda por Nombre");
        itemList1.setOrden("0");
        itemList1.setList(listEntidadBusqueda);
        itemList1.setAttibute("titulo");
        list.add(itemList1);
        view.mostrarLibreriaBusqueda(list,1);

    }

    public void validarRepetido(EntidadBusqueda entidadBusqueda){
        PedidoRepository pedidoRepository = new PedidoDataRepository(view.getContext());
        GetPedidoUseCase getPedidoUseCase = new GetPedidoUseCaseImpl(pedidoRepository);
        Pedido pedido = getPedidoUseCase.ejecutar();

        ProductoRepository productoDataRepository = new ProductoDataRepository();
        ListarProductoUseCase listarProductoUseCase = new ListarProductoUseCaseImpl(productoDataRepository);
        List<Producto> listaProductos = listarProductoUseCase.ejecutar();

        Producto item = new Producto();
        for (Producto producto:listaProductos){
            if(producto.getCodigoProducto().equalsIgnoreCase(entidadBusqueda.getDetalle())){
                item = producto;
                item.setEstado(1);
                item.setDescuento("12.3");
                item.setBonificacion("6.0");
                item.setStock("13");
            }
        }

        view.irProductoDetalle(item);



    }

}
