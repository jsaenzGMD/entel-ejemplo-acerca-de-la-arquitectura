package pe.entel.android.acerosarequipa.view;

/**
 * Created by jsaenz on 6/11/2017.
 */

public interface LoginView extends BaseView {

    void actualizarVersion(String version);
    void irPreferenciar(int perfil);
    void irMenu();
    void actualizarUsuario(String codigo,String clave);
}
