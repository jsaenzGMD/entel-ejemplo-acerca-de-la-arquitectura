package pe.entel.android.acerosarequipa.presenter;


import pe.entel.android.acerosarequipa.data.repository.OperadorDataRepository;
import pe.entel.android.acerosarequipa.data.repository.UtilDataRepository;
import pe.entel.android.acerosarequipa.domain.repository.OperadorRepository;
import pe.entel.android.acerosarequipa.domain.repository.UtilRepository;
import pe.entel.android.acerosarequipa.domain.usercase.ValidarOperadorUseCase;
import pe.entel.android.acerosarequipa.domain.usercase.implementation.ValidarOperadorUseCaseImpl;
import pe.entel.android.acerosarequipa.view.BaseView;

/**
 * Created on 07/04/2017.
 */
public class BasePresenter {

    final BaseView view;
    public BasePresenter(BaseView view) {
        this.view = view;
    }

    public void iniciar() {
        validarOperador();
        validarSubscriptor();
    }

    private void validarOperador() {
        UtilRepository utilRepository = new UtilDataRepository(view.getContext());
        OperadorRepository operadorRepository = new OperadorDataRepository(view.getContext());
        ValidarOperadorUseCase validarOperadorUseCase = new ValidarOperadorUseCaseImpl(operadorRepository,utilRepository);

        try {
            validarOperadorUseCase.ejecutar();
        }catch (Exception e){
            view.irOperadorInvalido();
        }

    }

    private void validarSubscriptor() {
        view.validarSubscriptor();
    }


}
