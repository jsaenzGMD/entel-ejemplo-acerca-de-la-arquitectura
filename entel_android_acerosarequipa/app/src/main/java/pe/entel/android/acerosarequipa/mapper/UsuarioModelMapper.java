package pe.entel.android.acerosarequipa.mapper;

import org.modelmapper.ModelMapper;

import pe.entel.android.acerosarequipa.domain.entity.Usuario;
import pe.entel.android.acerosarequipa.model.UsuarioModel;

/**
 * Created by jsaenz on 6/11/2017.
 */

public final class UsuarioModelMapper {
    public static UsuarioModel adapter(Usuario usuario){
        UsuarioModel usuarioModel = new UsuarioModel();
        if(usuario != null){
            ModelMapper modelMapper = new ModelMapper();
            usuarioModel = modelMapper.map(usuario,UsuarioModel.class);
        }
        return  usuarioModel;
    }
}
