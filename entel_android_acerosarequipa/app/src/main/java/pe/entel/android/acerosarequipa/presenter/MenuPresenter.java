package pe.entel.android.acerosarequipa.presenter;

import pe.entel.android.acerosarequipa.data.exception.NetworkConnectionException;
import pe.entel.android.acerosarequipa.data.repository.MaestroDataRepository;
import pe.entel.android.acerosarequipa.data.repository.UsuarioDataRepository;
import pe.entel.android.acerosarequipa.domain.exception.ErrorBundle;
import pe.entel.android.acerosarequipa.domain.repository.MaestroRespository;
import pe.entel.android.acerosarequipa.domain.repository.UsuarioRepository;
import pe.entel.android.acerosarequipa.domain.usercase.GetUsuarioUseCase;
import pe.entel.android.acerosarequipa.domain.usercase.ObtenerMaestrosUseCase;
import pe.entel.android.acerosarequipa.domain.usercase.implementation.GetUsuarioUseCaseImpl;
import pe.entel.android.acerosarequipa.domain.usercase.implementation.ObtenerMaestrosUseCaseImpl;
import pe.entel.android.acerosarequipa.mapper.UsuarioModelMapper;
import pe.entel.android.acerosarequipa.model.UsuarioModel;
import pe.entel.android.acerosarequipa.view.activity.MenuActivity;

/**
 * A cada uno de los activities le corresonderá un "presenter"
 */

public class MenuPresenter {
    /**
     * Instanciamos la clase principal
     */
    MenuActivity view;

    public MenuPresenter(MenuActivity view) {
        this.view = view;
    }

    /**
     * Permite invocar "PedidoActivity"
     */
    public void iniciarPedido(){
        /**
         * la implementación de este método se encuentra dentro de "MenuActivity",
         */
        view.irPedido();
    }

    public void sincronizarDatosMaestros() {
        UsuarioRepository usuarioRepository = new UsuarioDataRepository(view.getContext());
        GetUsuarioUseCase getUsuarioUseCase = new GetUsuarioUseCaseImpl(usuarioRepository);
        UsuarioModel usuario = UsuarioModelMapper.adapter(getUsuarioUseCase.ejecutar());

        /**
         * Para el consumo de un webservice, se deberá instanciar las interfaces y la implamentación de los métodos que se encuentren en
         * esta interfaz se realizará dentro del módulo de data.
         */
        MaestroRespository maestroRepository = new MaestroDataRepository(view.getContext());
        ObtenerMaestrosUseCase obtenerMaestrosUseCase = new ObtenerMaestrosUseCaseImpl(maestroRepository);
        obtenerMaestrosUseCase.ejecutar(usuario.getId(), new ObtenerMaestrosUseCase.CallbackObtenerMaestrosUseCase() {
            @Override
            public void onSincronizado(String resultado) {
                view.hideloading();
                view.showCorrect(resultado);
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                String mensaje = errorBundle.getErrorMessage();

                if (mensaje == null || mensaje.equals("")) {
                    mensaje = errorBundle.getException().getClass().getName();

                    if (errorBundle.getException().getClass().isInstance(new NetworkConnectionException())) {
                        mensaje = "Fuera de cobertura";
                    }
                }
                view.showError(mensaje);
            }
        });

        view.showloading("Obteniendo Maestros...");




    }
}
