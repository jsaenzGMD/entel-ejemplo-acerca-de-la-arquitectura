package pe.entel.android.acerosarequipa.view.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import pe.entel.android.acerosarequipa.R;
import pe.entel.android.acerosarequipa.navigation.Navigator;
import pe.entel.android.acerosarequipa.presenter.MenuPresenter;
import pe.entel.android.acerosarequipa.view.MenuView;

public class MenuActivity extends BaseActivity implements MenuView, View.OnClickListener {

    @InjectView(R.id.ll_ingresar_pedido)
    LinearLayout ll_ingresarPedido;

    MenuPresenter menuPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.inject(this);
        ll_ingresarPedido.setOnClickListener(this);
        /**
         * Se instancia el menú presenter, permitirá ingresar a los distintos eventos que se encuentran en esta clase,
         */
        menuPresenter = new MenuPresenter(this);

    }

    /**
     * Evento que permite invocar el evento realizado en el navigator
     */
    @Override
    public void irPedido() {
        Navigator.navigateToPedido(this);
    }


    @Override
    public void onClick(View v) {
        if(v.getId() ==R.id.ll_ingresar_pedido){
            sincronizarDatosMaestros();
            ingresarPedido();
        }
    }

    private void sincronizarDatosMaestros() {
        menuPresenter.sincronizarDatosMaestros();
    }

    public void ingresarPedido(){
        menuPresenter.iniciarPedido();
    }

    public static Intent getCallingIntent(Context context){
        return new Intent(context,MenuActivity.class);
    }

}
