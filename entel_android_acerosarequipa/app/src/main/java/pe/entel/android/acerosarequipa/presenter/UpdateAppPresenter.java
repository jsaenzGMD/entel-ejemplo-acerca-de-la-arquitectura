package pe.entel.android.acerosarequipa.presenter;


import pe.entel.android.acerosarequipa.data.exception.NetworkConnectionException;
import pe.entel.android.acerosarequipa.data.repository.UpdateAppDataRepository;
import pe.entel.android.acerosarequipa.domain.exception.ErrorBundle;
import pe.entel.android.acerosarequipa.domain.repository.UpdateAppRepository;
import pe.entel.android.acerosarequipa.domain.usercase.UpdateAppUseCase;
import pe.entel.android.acerosarequipa.domain.usercase.implementation.UpdateAppUseCaseImpl;
import pe.entel.android.acerosarequipa.view.UpdateAppView;

/**
 * Created by rtamayov on 03/04/2017.
 */
public class UpdateAppPresenter {

    final UpdateAppView view;

    public UpdateAppPresenter(UpdateAppView view) {
        this.view = view;
   }

    public void updateApp(String url) {
        view.showloading("Descargando actualizacion...");
        UpdateAppRepository updateAppRepository =  new UpdateAppDataRepository(view.getContext());
        UpdateAppUseCase updateAppUseCase = new UpdateAppUseCaseImpl(updateAppRepository);


        updateAppUseCase.updateApp(url, new UpdateAppUseCase.Callback() {
            @Override
            public void onUpdatedApp() {
                view.hideloading();
                view.finalizar();
            }

            @Override
            public void onError(ErrorBundle errorBundle) {
                String mensaje = errorBundle.getErrorMessage();

                if (mensaje == null || mensaje.equals("")){
                    mensaje = errorBundle.getException().getClass().getName();

                    if (errorBundle.getException().getClass().isInstance(new NetworkConnectionException())){
                        mensaje = "Fuera de cobertura";
                    }
                }

                view.hideloading();
                view.showError(mensaje);
                view.finalizar();
            }
        });
    }
}
