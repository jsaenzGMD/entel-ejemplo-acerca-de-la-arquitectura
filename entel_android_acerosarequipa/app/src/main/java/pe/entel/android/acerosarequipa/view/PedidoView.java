package pe.entel.android.acerosarequipa.view;

import java.util.List;

import pe.entel.android.acerosarequipa.domain.entity.Producto;
import pe.entel.android.controlesentel.busqueda.model.ListaBusqueda;

/**
 * Created by jsaenz on 7/11/2017.
 */

public interface PedidoView extends BaseView {

    void actualizarCabecera(int cantidadProductos,int totalItems, int montoTotal);
    void mostrarLibreriaBusqueda(List<ListaBusqueda> list, int tipoList);
    void irProductoDetalle(Producto producto);
}
