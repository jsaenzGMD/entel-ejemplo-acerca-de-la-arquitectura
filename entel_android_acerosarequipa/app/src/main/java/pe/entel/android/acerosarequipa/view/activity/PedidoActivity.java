package pe.entel.android.acerosarequipa.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import pe.entel.android.acerosarequipa.R;
import pe.entel.android.acerosarequipa.domain.entity.Producto;
import pe.entel.android.acerosarequipa.navigation.Navigator;
import pe.entel.android.acerosarequipa.presenter.PedidoPresenter;
import pe.entel.android.acerosarequipa.view.PedidoView;
import pe.entel.android.controlesentel.busqueda.model.EntidadBusqueda;
import pe.entel.android.controlesentel.busqueda.model.ListaBusqueda;

public class PedidoActivity extends BaseActivity implements PedidoView, View.OnClickListener {
    /**
     * Permite instanciar el presenter.
     */
    PedidoPresenter pedidoPresenter;

    @InjectView(R.id.btnAgregarProducto)
    Button btnAgregarProducto;
    @InjectView(R.id.btnEnviarPedido)
    Button btnEnviarPedido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido);
        ButterKnife.inject(this);
        /**
         * Ingresar el contexto actual hacia el presenter
         */
        pedidoPresenter = new PedidoPresenter(this);
        btnAgregarProducto.setOnClickListener(this);
        btnEnviarPedido.setOnClickListener(this);
    }

    @Override
    public void actualizarCabecera(int cantidadProductos, int totalItems, int montoTotal) {

    }

    @Override
    public void mostrarLibreriaBusqueda(List<ListaBusqueda> list, int tipoList) {
        Navigator.navigateToProductoLista(this,list,tipoList);
    }

    @Override
    public void irProductoDetalle(Producto producto) {
        
    }

    /**
     * A través de este método permite invocar el activity desde el Navigator
     * @param context
     * @return
     */
    public static Intent getCallingIntent(Context context){
        return new Intent(context,PedidoActivity.class);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnAgregarProducto:
                pedidoPresenter.irListaProductos();
                break;
            case R.id.btnEnviarPedido:
                ;break;
        }

    }

    /**
     * Recibiendo los datos desde el componente de la librería
     */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 &&
                resultCode == RESULT_OK) {

            Bundle bundle = data.getExtras();
            int tipoList = bundle.getInt("tipoList");
            int tipoClick = bundle.getInt("tipoClick");
            EntidadBusqueda item = (EntidadBusqueda) bundle.getSerializable("item");
            //int position = bundle.getInt("position");

            pedidoPresenter.validarRepetido(item);
        }
    }
}
