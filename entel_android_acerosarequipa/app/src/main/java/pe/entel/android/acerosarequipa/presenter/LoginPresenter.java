package pe.entel.android.acerosarequipa.presenter;

import pe.entel.android.acerosarequipa.R;
import pe.entel.android.acerosarequipa.data.exception.NetworkConnectionException;
import pe.entel.android.acerosarequipa.data.repository.UsuarioDataRepository;
import pe.entel.android.acerosarequipa.data.repository.VersionDataRepository;
import pe.entel.android.acerosarequipa.domain.exception.ErrorBundle;
import pe.entel.android.acerosarequipa.domain.repository.UsuarioRepository;
import pe.entel.android.acerosarequipa.domain.repository.VersionRepository;
import pe.entel.android.acerosarequipa.domain.usercase.GetUsuarioUseCase;
import pe.entel.android.acerosarequipa.domain.usercase.GetVersionUseCase;
import pe.entel.android.acerosarequipa.domain.usercase.ValidarUsuarioUseCase;
import pe.entel.android.acerosarequipa.domain.usercase.implementation.GetUsuarioUseCaseImpl;
import pe.entel.android.acerosarequipa.domain.usercase.implementation.GetVersionUseCaseImpl;
import pe.entel.android.acerosarequipa.domain.usercase.implementation.ValidarUsuarioUseCaseImpl;
import pe.entel.android.acerosarequipa.mapper.UsuarioModelMapper;
import pe.entel.android.acerosarequipa.model.UsuarioModel;
import pe.entel.android.acerosarequipa.util.EntelConfig;
import pe.entel.android.acerosarequipa.util.UtilitarioApp;
import pe.entel.android.acerosarequipa.view.LoginView;

/**
 * Created on 6/11/2017.
 */

public class LoginPresenter {
    LoginView view;

    public LoginPresenter(LoginView view) {
        this.view = view;
    }

    public void iniciar(){
        obtenerVersion();
        verificarUsuario();
    }

    private void verificarUsuario() {
        UsuarioRepository usuarioRepository = new UsuarioDataRepository(view.getContext());
        GetUsuarioUseCase getUsuarioUseCase = new GetUsuarioUseCaseImpl(usuarioRepository);
        UsuarioModel usuario = UsuarioModelMapper.adapter(getUsuarioUseCase.ejecutar());

        if(usuario!=null) {
            view.actualizarUsuario(usuario.getCodigo(), usuario.getClave());
        }
    }

    private void obtenerVersion() {
        VersionRepository versionRepository = new VersionDataRepository(view.getContext());
        GetVersionUseCase getVersionUseCase = new GetVersionUseCaseImpl(versionRepository);

        String suite = getVersionUseCase.ejecutar();
        String apk = UtilitarioApp.fnVersion(view.getContext());
        String version = "APK:" + apk + " - Suite:" + suite;

        view.actualizarVersion(version);
    }

    public void validarUsuario(String codigo, String clave) {
        if (!isExportarBD(codigo, clave)) {
            UsuarioRepository usuarioRepository = new UsuarioDataRepository(view.getContext());
            ValidarUsuarioUseCase validarUsuarioUseCase = new ValidarUsuarioUseCaseImpl(usuarioRepository);

            view.showloading("Validando Usuario");

            validarUsuarioUseCase.ejecutar(codigo, clave, new ValidarUsuarioUseCase.Callback() {
                @Override
                public void onValidado(String mensaje) {
                    view.hideloading();
                    view.showCorrect(mensaje);
                    view.irMenu();
                }

                @Override
                public void onError(ErrorBundle errorBundle) {
                    view.hideloading();
                    String mensaje = errorBundle.getErrorMessage();

                    if (mensaje == null || mensaje.equals("")) {
                        mensaje = errorBundle.getException().getClass().getName();

                        if (errorBundle.getException().getClass().isInstance(new NetworkConnectionException())) {
                            mensaje = "Fuera de cobertura";
                        }
                    }
                    view.showError(mensaje);
                }
            });
        }
    }

    private boolean isExportarBD(String usuario, String clave) {

        if (!usuario.equals("")) {
            return false;
        }

        if (clave.equals(view.getContext().getString(R.string.keyexportbd_password))) {
            UtilitarioApp.ExportarBD(view.getContext());
            return true;
        }
        return false;
    }

    public void validarPerfil(String usuario, String clave) {
        String password = view.getContext().getString(R.string.keypreadm_password);
        int perfil;

        if (usuario.equals(password) && clave.equals(password)) {
            perfil = EntelConfig.EnumRolPreferencia.ADMINISTRADOR.ordinal();
        } else {
            perfil = EntelConfig.EnumRolPreferencia.USUARIO.ordinal();
        }

        view.irPreferenciar(perfil);
    }
}
